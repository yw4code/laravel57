<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('user/create', 'Api\UserController@create')->middleware(['required:name,email,password']);
Route::post('loan/create', 'Api\LoanController@create')->middleware(
    ['required:id_number,account,credit,interest_rate,arrangement_fee,duration_start,duration_end,repayment_frequency']
);
Route::post('repayment/create', 'Api\RepaymentController@create')->middleware(
    ['required:id_number,loan_id_number,credit']
);