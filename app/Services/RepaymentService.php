<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 2019/2/26
 * Time: 下午10:57
 */
namespace App\Services;

use App\Exceptions\ApiException;
use App\Loan;
use App\Repayment;
use App\User;
use Illuminate\Http\Request;

class RepaymentService
{
    public function validate(Loan $loan) {

        $request = request();

        if (Repayment::where('id_number', $request->id_number)->first() != null)
            throw new ApiException(ApiException::DATA_EXISTS, ['id_number']);

        if ($loan == null)
            throw new ApiException(ApiException::LOAN_NOT_FOUND, [$request->loan_id_number]);

        // validate repayment_status
        if ($loan->repayment_status == 'finished')
            throw new ApiException(ApiException::LOAN_FINISHED, [$request->loan_id_number]);

        // validate duration
        if ((strtotime($loan->duration_start)>time()) || (strtotime($loan->duration_end)<time()))
            throw new ApiException(ApiException::OUT_OF_REPAY_DURATION);

    }
}