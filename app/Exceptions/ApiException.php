<?php
namespace App\Exceptions;

class ApiException extends \Exception {

    const USER_NOT_FOUND = ['code' => 5, 'message' => 'Account %s is not found'];
    const PARAMS_REQUIRED = ['code' => 6, 'message' => 'Param : %s is required'];
    const DATA_EXISTS = ['code' => 7, 'message' => ' %s already exists'];
    const LOAN_NOT_FOUND = ['code' => 8, 'message' => 'loan id number:  %s is not found'];
    const LOAN_FINISHED = ['code' => 9, 'message' => 'loan id number:  %s is already finished'];
    const OUT_OF_REPAY_DURATION = ['code' => 10, 'message' => 'Out of repay duration'];

    protected $code;
    protected $message;

    public function __construct(array $error, $params = [])
    {
        $this->message = vsprintf($error['message'], $params);
        $this->code = $error['code'];
    }

    public function getResponse() {
        return response()
            ->json(['code' => $this->code, 'message' => $this->message], 400);
    }
}