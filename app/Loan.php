<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $guarded = [];

    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function repayments() {
        return $this->hasMany(Repayment::class, 'loan_id', 'id');
    }
}
