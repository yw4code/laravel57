<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{

    public function create(Request $request) {

        if (User::where('email', $request->email)->first() != null)
            throw new ApiException(ApiException::DATA_EXISTS, ['email']);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);

        return response(['user_id'=> $user->id], 201);
    }
}
