<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use App\Loan;
use App\Repayment;
use App\Services\RepaymentService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RepaymentController extends Controller
{
    /**
     * @param Request $request
     *      account
     *      loan_id_number
     *      credit
     * @return Response
     * @throws ApiException
     * @throws
     */
    public function create(Request $request, RepaymentService $repaymentService) {

        $loan = Loan::where('id_number', $request->loan_id_number)->first();

        $repaymentService->validate($loan);


        $sumOfRepaidCredit = Repayment::where('loan_id', $loan->id)
            ->select(\DB::raw('SUM(credit) as totalCredit'))
            ->first()->totalCredit;

        $sumOfRepaidCredit+=$request->credit;

        \DB::beginTransaction();

        try {

            // already repaid all credit with interest
            if ($sumOfRepaidCredit >= ($loan->credit + bcmul($loan->credit,$loan->interest_rate,8)) ) {
                $loan->repayment_status = 'finished';
                $loan->save();
            }

            $repayment = Repayment::create([
                'id_number' => $request->id_number,
                'loan_id' => $loan->id,
                'credit' => $request->credit,
            ]);

        } catch (\Exception $exception) {
            \DB::rollback();
            throw $exception;
        }

        \DB::commit();


        return response([
            'repayment_id'=> $repayment->id,
            'sum_repaid_credit' => $sumOfRepaidCredit,
        ], 201);
    }
}
