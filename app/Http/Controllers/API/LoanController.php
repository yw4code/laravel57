<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\ApiException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Loan;
use App\User;

class LoanController extends Controller
{
    /**
     * @param Request $request
     *      account
     *      id_number
     *      ...
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function create(Request $request) {

        $user = User::where('email', $request->account)->first();
        if ($user == null)
            throw new ApiException(ApiException::USER_NOT_FOUND, [$request->account]);

        if (Loan::where('id_number', $request->id_number)->first() != null)
            throw new ApiException(ApiException::DATA_EXISTS, ['id_number']);

        $loan = Loan::create([
            'id_number' => $request->id_number,
            'user_id' => $user->id,
            'credit' => $request->credit,
            'interest_rate'=> $request->interest_rate,
            'arrangement_fee'=> $request->arrangement_fee,
            'duration_start'=> $request->duration_start,
            'duration_end'=> $request->duration_end,
            'repayment_frequency'=> $request->repayment_frequency
        ]);

        return response(['loan_id'=> $loan->id], 201);
    }
}
