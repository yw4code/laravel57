<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\ApiException;

class ParamRequired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $numargs  = func_num_args();
        $params = [];
        for($i = 2; $i < $numargs; $i++)
        {
            $params[] = func_get_arg($i);
        }

        $required = $this->getRequiredParams($params, $request);

        foreach($required as $param)
        {
            if(!$request->has($param) || $request->get($param) === '')
            {
                throw new ApiException(ApiException::PARAMS_REQUIRED, [$param]);
            }
        }        
        return $next($request);
    }
    
    protected function getRequiredParams($params, $request){
        $required = array_merge($params);
        return $required;
    }      
}
