<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();

            $table->string('id_number');
            $table->decimal('credit', 20, 8);
            $table->decimal('interest_rate', 10, 8);
            $table->decimal('arrangement_fee');

            $table->dateTime('duration_start');
            $table->dateTime('duration_end');

            $table->integer('repayment_frequency')->comment('times of repayment');

            $table->timestamps();

            $table->enum('repayment_status', ['working', 'finished']);

            $table->index('id_number');
            $table->index('created_at');
            $table->index('duration_start');
            $table->index('duration_end');
        });


        Schema::table('loans', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
