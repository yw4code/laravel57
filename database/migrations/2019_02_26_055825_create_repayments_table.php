<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repayments', function (Blueprint $table) {
            $table->increments('id');

            $table->string('id_number');
            $table->integer('loan_id')->unsigned();
            $table->decimal('credit', 20, 8);

            $table->index('id_number');
            $table->index('created_at');

            $table->timestamps();
        });


        Schema::table('repayments', function (Blueprint $table) {
            $table->foreign('loan_id')
                ->references('id')->on('loans')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repayments');
    }
}
